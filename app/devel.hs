{-# LANGUAGE PackageImports #-}
import "werkerd" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
